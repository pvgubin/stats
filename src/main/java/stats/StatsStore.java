package stats;

import org.springframework.stereotype.Service;

@Service
public class StatsStore {

    private final static int GRANULARITY = 1;
    private final static int PERIOD = 60000;
    private final int ADJUSTED_PERIOD = 60000 / GRANULARITY;

    private StatsCollector collector = new StatsCollector(ADJUSTED_PERIOD);

    public Stats statistics() {
        return collector.getStats(System.currentTimeMillis() / GRANULARITY, ADJUSTED_PERIOD);
    }

    public void addStats(double amount, long timestamp) {
        collector.addStat(amount, timestamp / GRANULARITY);
    }
}
