package stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class StatsController {

    static class Request {
        public double amount;
        public long timestamp;
    }

    @Autowired
    StatsStore statsStore;

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public Stats statistics() {
        return statsStore.statistics();
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.POST)
    public void transactions(@RequestBody Request r, HttpServletResponse response) {
        if (System.currentTimeMillis() - r.timestamp > 60000)
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        statsStore.addStats(r.amount, r.timestamp);
    }
}
