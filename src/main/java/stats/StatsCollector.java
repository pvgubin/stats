package stats;

public class StatsCollector {
    static class Entry {
        public int count;
        public double sum;
        public double min;
        public double max;

        void init() {
            count = 0;
            sum = 0;
            min = 0;
            max = 0;
        }

        void update(double amount) {
            count++;
            sum += amount;
            min = min == 0 ? amount : Math.min(min, amount);
            max = Math.max(max, amount);
        }
    }

    private Entry[] buffer;
    private long curTimestamp = 0;
    private int cursor = 0;

    public StatsCollector(int size) {
        buffer = new Entry[size];
        for (int i = 0; i < size; i++) {
            buffer[i] = new Entry();
        }
    }

    public Stats getStats(long timestamp, int period) {
        if (period > buffer.length) throw new RuntimeException("Too long period");
        if (timestamp - curTimestamp > period) return new Stats(); // empty stats
        int cur = cursor;
        Stats stats = new Stats();
        synchronized (this) {
            for (int i = 0; i < curTimestamp - timestamp + period; i++) {
                updateStats(stats, buffer[cur]);
                cur = prev(cur);
            }
        }
        stats.avg = stats.sum / stats.count;
        return stats;
    }

    private int prev(int i, int c) {
        int res = i - c;
        return res >= 0 ? res : buffer.length + res;
    }

    private int prev(int i) {
        return prev(i, 1);
    }

    private int next(int i) {
        return (i + 1) % buffer.length;
    }

    private void updateStats(Stats stats, Entry entry) {
        stats.sum += entry.sum;
        stats.count += entry.count;
        stats.max = Math.max(stats.max, entry.max);
        stats.min = stats.min == 0 ? entry.min : Math.min(stats.min, entry.min);
    }

    public void addStat(double amount, long timestamp) {
        if (curTimestamp - timestamp > buffer.length) return;

        synchronized (this) {
            if (timestamp - curTimestamp > buffer.length) {
                initStats();
                curTimestamp = timestamp;
                buffer[cursor].update(amount);
            } else if (timestamp > curTimestamp) {
                // init all intermediate cells
                cursor = next(cursor);
                for (int i = 0; i < timestamp - curTimestamp; i++) {
                    buffer[cursor].init();
                }
                // set final cell
                buffer[cursor].update(amount);
                curTimestamp = timestamp;
            } else {
                // find corresponding cell and update it
                buffer[prev(cursor, (int) (curTimestamp - timestamp))].update(amount);
            }
        }
    }

    private void initStats() {
        for (Entry e: buffer) {
            e.init();
        }
        curTimestamp = 0;
        cursor = 0;
    }
}
