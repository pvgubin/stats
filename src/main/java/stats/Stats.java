package stats;

public class Stats {
    public double sum;
    public double avg;
    public double max;
    public double min;
    public long count;

    public Stats() {
        this(0, 0, 0, 0, 0);
    }

    public Stats(double sum, double avg, double max, double min, long count) {
        this.sum = sum;
        this.avg = avg;
        this.max = max;
        this.min = min;
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public double getAvg() {
        return avg;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public long getCount() {
        return count;
    }
}
