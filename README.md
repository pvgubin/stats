# README #

### To run/test ###

  * mvn spring-boot:run
  * curl -v -d '{"amount":10.3, "timestamp":1521791137212}' -H "Content-Type: application/json" -X POST http://localhost:8080/transactions
  * curl http://localhost:8080/statistics

### Notes ###

StatsCollector is implemented as a circular buffer. Given that the size of the buffer is limited all operations takes O(1). 

Ways to improve current solution:

  * Granularity of statistics is configurable. If it is not necessary to have millisecond granularity buffer size can be greatly reduced and the number of lookups as well. For example, for second granularity you can set GRANULARITY = 1000
  * Buffer can be updated in micro batches. If this way there will be less contention for lock.
  * Now all buffer access and manipulations are synchronised. To make it more scalable we can introduce copy on write buffer. In combination with micro batching it will eliminate contention fully.
  * We can save running totals instead of individual statistics in buffer. In this case buffer stats read will take at most one buffer lookup. But buffer management will be a bit more difficult.
